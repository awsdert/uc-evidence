#pragma once

#include "common.h"
#include "memory.h"
#include "stream.h"
#include "zlib.h"

#if !defined( __STDC_VERSION__ ) || __STDC_VERSION__  <= 199901L
#define __func__ __FUNCTION__
#endif

typedef struct _GZIP_FLAGS_STRUCT
{
	uint TEXT : 1;
	uint HCRC : 1;
	uint MORE : 1;
	uint NAME : 1;
	uint NOTE : 1;
	uint RESERVED : 3;
} GZIP_FLAGS_STRUCT;

typedef union _GZIP_FLAGS_UNION
{
	uint val;
	GZIP_FLAGS_STRUCT mem;
} GZIP_FLAGS_UNION;

typedef struct _GZIP_HEADER
{
	uint magic;
	uint format;
	GZIP_FLAGS_UNION flags;
	ulong mtime;
	uint xflags;
	uint system;
} GZIP_HEADER;

typedef struct _GZIP
{
	STREAM_BUFFER_ID Data;
	int Into;
	size_t deflated_size;
	STREAM Stream;
	GZIP_HEADER header;
	ZLIB zlib;
} GZIP;

int SetupGzipObject( GZIP *gzip, BUFFERS *buffer );
int LoadGzipArchive( GZIP *gzip, char const *path );
void LoadGzipDetails( GZIP *gzip, ulong end32bits );
int ExpandGzipArchive( GZIP *gzip );
