#ifndef INC_STREAM_H
#define INC_STREAM_H

#include "common.h"
#include "memory.h"

typedef struct _STREAM_BUFFER_ID
{
	int id;
	BUFFERS *Buffers;
} STREAM_BUFFER_ID;

typedef long (*StreamBuffCB)( void *dst, void *ptr, long pos, long bytes );

long StreamDataFromBufferObject( void *dst, void *ptr, long pos, long bytes );
long StreamDataFromBufferIndex( void *dst, void *ptr, long pos, long bytes );

typedef struct _STREAM
{
	bool		eod;
	int 		err;
	long		byte;
	void *		data;
	StreamBuffCB DataCB;
	intmax_t	used;
	intmax_t	took;
	uchar		feed[BUFSIZ];
} STREAM;

void	  	StreamUsed( STREAM *Stream, uint add );
int			StreamData( STREAM *Stream, void *dst, intmax_t get, bool use );
uintmax_t	StreamBits( STREAM *Stream, uint get, bool use );
void		EchoStreamDetails( FILE *file, STREAM *Stream );

void InitDataStream( STREAM *Stream, void *data, uint bit );
void InitFileStream( STREAM *Stream, FILE *file );

#endif
