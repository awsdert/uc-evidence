#include "stream.h"

long StreamDataFromBufferObject( void *dst, void *ptr, long pos, long get )
{
	BUFFER *Data = ptr;
	uchar *data = Data->addr;
	long upto = Data->used - pos;
	upto = (upto > 0) ? upto : 0;
	get = (get <= upto) ? get : upto;
	memcpy( dst, data + pos, get );
	return get;
}

long StreamDataFromBufferIndex( void *dst, void *ptr, long pos, long get )
{
	STREAM_BUFFER_ID *Id = ptr;
	BUFFER *Data = AccessBuffer( Id->Buffers, Id->id );
	return StreamDataFromBufferObject( dst, Data, pos, get );
}

void EchoBits( FILE *file, void const *src, intmax_t pos, intmax_t num )
{
	intmax_t max;
	BIT b;

	if ( pos < 0 )
		return;

	max = pos + num;

	for ( SetBit( &b, src, max - 1 ); b.abs >= pos; DecBit( &b, src ) )
		fputc( '0' + !!(*(b.byte) & b.mask), file );
}

void EchoBitDetails( FILE *dst, BIT *bit )
{
	fprintf
	(
		dst,
		"Bit Details: abs = %" PRIdMAX ", "
		"pos = %ld, rel = %ld, byte = %p\n",
		bit->abs,
		bit->pos,
		bit->rel,
		(void*)(bit->byte)
	);
}

void SetBit( BIT *bit, void const *root, long abs )
{
	uchar const *base = root;
	bit->abs = abs;
	bit->pos = abs / CHAR_BIT;
	bit->rel = abs % CHAR_BIT;
	bit->mask = ((uintmax_t)1) << bit->rel;
	bit->byte = base + bit->pos;
}

void ForeBits( void *DST, void const *SRC, long POS, long GET )
{
	long got;
	BIT d = {0}, s;

	SetBit( &d, DST, 0 );
	SetBit( &s, SRC, POS );

	for ( got = 0; got < GET; ++got )
	{
		*((uchar*)(d.byte)) |= (*(s.byte) & s.mask) ? d.mask : 0;
		IncBit( &d, DST );
		IncBit( &s, SRC );
	}
}

void BackBits( void *DST, void const *SRC, long POS, long GET )
{
	long got;
	BIT d = {0}, s;

	SetBit( &d, DST, GET - 1 );
	SetBit( &s, SRC, POS );

	for ( got = 0; got < GET; ++got )
	{
		*((uchar*)(d.byte)) |= (*(s.byte) & s.mask) ? d.mask : 0;
		DecBit( &d, DST );
		DecBit( &s, SRC );
	}
}

uintmax_t GetBits( void const * data, uint pos, uint bits, bool backwards )
{
	uintmax_t value = 0;

	bits = (bits < bitsof(uintmax_t)) ? bits : bitsof(uintmax_t);

	if ( backwards )
		BackBits( &value, data, pos, bits );
	else
		ForeBits( &value, data, pos, bits );

	return  value;
}

void FlipBits( void *SRC, intmax_t num )
{
	BIT d, s;

	SetBit( &d, SRC, 0 );
	SetBit( &s, SRC, num - 1 );

	while ( d.abs < s.abs )
	{
		/* Store state */
		uchar dst = *(d.byte) & d.mask;
		uchar src = *(s.byte) & s.mask;

		/* Clear bit */
		*((uchar*)(d.byte)) &= ~(d.mask);
		*((uchar*)(s.byte)) &= ~(s.mask);

		/* Use state to set bit */
		*((uchar*)(d.byte)) |= src ? d.mask : 0;
		*((uchar*)(s.byte)) |= dst ? s.mask : 0;

		IncBit( &d, SRC );
		DecBit( &s, SRC );
	}
}

void StreamUsed( STREAM *Stream, uint bits )
{
	intmax_t max = BUFSIZ;
	max *= CHAR_MAX;
	Stream->took += bits;
	Stream->used += bits;

	if ( Stream->eod && Stream->took >= max )
		Stream->err = EOF;
}

long StreamBlock( STREAM * Stream )
{
	void *src = Stream->feed;
	long add = -1;

	memset( src, 0, BUFSIZ );

	if ( Stream->eod )
		Stream->err = EOF;
	else
	{
		add = Stream->DataCB( src, Stream->data, Stream->byte, BUFSIZ );
		Stream->eod = (add < BUFSIZ);
	}

	return add;
}

int StreamData( STREAM *Stream, void *dst, intmax_t get, bool use )
{
	BIT d, s;
	intmax_t const max = ((intmax_t)BUFSIZ) * CHAR_BIT;
	intmax_t num = max - Stream->took;
	void * src = Stream->feed;

	SetBit( &d, dst, 0 );

	if ( Stream->byte < 1 )
	{
		Stream->took = 0;
		num = StreamBlock( Stream );

		if ( num < 0 )
			return EOF;

		if ( !num )
			return ENODATA;

		Stream->byte = num;
		num *= CHAR_BIT;
	}

	while ( d.abs < get )
	{
		intmax_t take = get - d.abs, bits = Stream->took;

		while ( bits >= max )
		{
			num = StreamBlock( Stream );

			if ( num < 0 )
				return EOF;

			if ( !num )
				return ENODATA;

			Stream->byte += num;
			num *= CHAR_BIT;
			bits -= max;
			Stream->took = 0;

			EchoStreamDetails( stdout, Stream );
		}

		SetBit( &s, src, Stream->took );

		if ( num < take )
			take = num;

		for ( bits = 0; bits < take; ++bits )
		{
			if ( *(s.byte) & s.mask )
				*((uchar*)(d.byte)) |= d.mask;

			IncBit( &d, dst );
			IncBit( &s, src );
		}

		if ( use )
			StreamUsed( Stream, take );
		else if ( get > num )
			return ERANGE;
	}

	return 0;
}

uintmax_t StreamBits( STREAM *Stream, uint get, bool use )
{
	uintmax_t val = 0;

	if ( get > bitsof(uintmax_t) )
	{
		Stream->err = ERANGE;
		return 0;
	}

	Stream->err = StreamData( Stream, &val, get, use );
	return val;
}

void EchoStreamDetails( FILE *file, STREAM *Stream )
{
	fprintf
	(
		file,
		"Stream Details: "
		"End Of Data = %5s, "
		"End Of Stream = %5s, "
		"data = %p, "
		"byte = %ld, "
		"took = %" PRIdMAX ", "
		"used = %" PRIdMAX ", "
		"used / 8 = %" PRIdMAX "\n",
		BOOL2STR( Stream->eod ),
		BOOL2STR( Stream->err == EOF ),
		Stream->data,
		Stream->byte,
		Stream->took,
		Stream->used,
		Stream->used / 8
	);
}
