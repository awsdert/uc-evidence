#ifndef INC_ZLIB_H
#define INC_ZLIB_H

#include "common.h"
#include "memory.h"
#include "stream.h"

extern bool quiet_zlib;

typedef struct _ZLIB_SYMBOL
{
	uint uid;
	uint src;
	uint val;
	uint sym;
	uint lit;
	uint len;
	uint get;
	uint cpy;
	uint use;
	uint nxt[2];
} ZLIB_SYMBOL;

typedef struct _ZLIB_IMPLIED
{
	uint get[32];
	uint cpy[32];
} ZLIB_IMPLIED;

#if 0
extern ZLIB_IMPLIED implied_type_data;
extern ZLIB_IMPLIED implied_code_data;
extern ZLIB_IMPLIED implied_dist_data;
#endif

typedef struct _ZLIB_SYMBOLS
{
	int foresee;
	uint longest;
	uint highest;
	int entries;
	BUFFERS *Buffers;
} ZLIB_SYMBOLS;

typedef enum _ZLIB_SYMBOLS_ID
{
	ZLIB_SYMBOLS_ID_TYPE = 0,
	ZLIB_SYMBOLS_ID_CODE,
	ZLIB_SYMBOLS_ID_LOOP,
	ZLIB_SYMBOLS_ID_COUNT
} ZLIB_SYMBOLS_ID;

typedef struct _ZLIB
{
	int Into;
	uint method;
	uint xflags;
	STREAM *Stream;
	BUFFERS *Buffers;
	ZLIB_SYMBOLS Symbols[ZLIB_SYMBOLS_ID_COUNT];
} ZLIB;

typedef int (*ExpandZlibStreamTypeCB)( ZLIB *zlib );

int ExpandZlibStreamType0( ZLIB *zlib );
int ExpandZlibStreamType1( ZLIB *zlib );
int ExpandZlibStreamType2( ZLIB *zlib );
int ExpandZlibStreamType3( ZLIB *zlib );
int ExpandZlibStream( ZLIB *zlib, STREAM *Stream, int IntoID );

extern ExpandZlibStreamTypeCB ExpandZlibStreamType[4];

void EchoZlibDetails( ZLIB *zlib );
void InitZlibSymbolUIDs( ZLIB_SYMBOLS *Symbols );
void * ExpandZlibSymbolsBuffer( ZLIB_SYMBOLS *Symbols, uint want );
ZLIB_SYMBOL * SeekZlibSymbol( STREAM *Stream, ZLIB_SYMBOLS *Symbols );
int SortZlibSymbolsByUID( ZLIB_SYMBOLS *Symbols );

void * LoadZlibTypes( ZLIB *zlib );
void * LoadZlibHuffs( ZLIB *zlib, uint starting_lit );

void EchoZlibSymbolDetails( FILE *dst, ZLIB_SYMBOL *symbol, uint pos );
void EchoZlibSymbolsListDetails( FILE *dst, ZLIB *zlib, int id );

int SetupZlibObject( ZLIB *zlib, BUFFERS *Buffers );
void ClearZlibObject( ZLIB *zlib, STREAM *Stream, int IntoID );

#endif
