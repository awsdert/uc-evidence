#ifndef INC_MEMORY_H
#define INC_MEMORY_H

#include "common.h"

typedef void * (*AllocCB)( void *ud, void *ptr, size_t size, size_t want );

typedef struct _ALLOC
{
	void *ud;
	AllocCB alloc;
	FILE *errors, *verbose, *detailed;
} ALLOC;

void * default_allocator( void *ud, void *ptr, size_t size, size_t want );

/* DO NOT USE in anything but main thread, even then only if you have properly
 * taken care of race conditions or are operating in single threaded mode
 * because under certain conditions malloc & co &/or re-implimentations of them
 * will be writing to globals without locks which is not thread safe */
extern ALLOC DefaultAlloc;

/* pos = abs / CHAR_BIT, rel = abs % CHAR_BIT */
typedef struct _BIT
{
	intmax_t abs;
	long pos;
	long rel;
	uchar const * byte;
	uchar	mask;
} BIT;

void SetBit( BIT *b, void const *root, intmax_t abs );
#define AddBit( BIT, ROOT, ABS ) SetBit( BIT, ROOT, (BIT)->abs + (ABS) )
#define SubBit( BIT, ROOT, ABS ) SetBit( BIT, ROOT, (BIT)->abs + (ABS) )
#define IncBit( BIT, ROOT ) SetBit( BIT, ROOT, (BIT)->abs + 1 )
#define DecBit( BIT, ROOT ) SetBit( BIT, ROOT, (BIT)->abs - 1 )

void EchoBits( FILE *file, void const *src, intmax_t pos, intmax_t num );
void ForeBits( void *DST, void const *SRC, intmax_t pos, intmax_t num );
void BackBits( void *DST, void const *SRC, intmax_t pos, intmax_t num );
void FlipBits( void *SRC, intmax_t num );

typedef struct _BUFFER
{
	ALLOC *Alloc;
	size_t size;
	int have;
	int used;
	void *addr;
} BUFFER, BOOLEANS;

typedef struct _BINARY
{
	BIT Used, Have;
	BUFFER Buffer;
} BINARY;

void * ExpandBinary( struct _BINARY *Binary, intmax_t want );
void * ShrinkBinary( struct _BINARY *Binary, intmax_t want );

typedef struct _SERIES
{
	ALLOC *Alloc;
	BINARY Active;
	BUFFER Buffer;
} SERIES, BUFFERS;

void EchoBufferDetails( FILE *file, BUFFER *Buffer );
void * ExpandBuffer( struct _BUFFER *Buffer, int size, int want );
void * ShrinkBuffer( struct _BUFFER *Buffer, int size, int want );

/* Returns errno type code */
void EmptyBuffers( BUFFERS *Buffers );
int ExtraBuffers( BUFFERS *Buffers, int want );
/* Returns index */
int ObtainBuffer( BUFFERS *Buffers );
void GiveUpBuffer( BUFFERS *Buffers, int i );
/* Does NOT check if in bounds, e.g. buffer_list_ptr + i */
BUFFER * AccessBuffer( BUFFERS *Buffers, int i );

#endif
