#include "memory.h"

void * default_allocator( void *ud, void *ptr, size_t had, size_t get )
{
	uchar *tmp;
	(void)ud;

	if ( !get )
	{
		if ( ptr )
			free( ptr );

		return NULL;
	}

	tmp = ptr ? realloc( ptr, get ) : malloc( get );

	if ( !tmp )
		return NULL;

	if ( get > had )
		memset( tmp + had, 0, get - had );

	return tmp;
}

ALLOC DefaultAlloc = { NULL, default_allocator, NULL, NULL, NULL };

void EchoBufferDetails( FILE *file, BUFFER *Buffer )
{
	fprintf
	(
		file,
		"Buffer Details: addr = %p, "
		"used = %d, have = %d, size = %" PRIuMAX "\n",
		Buffer->addr, Buffer->used, Buffer->have, (uintmax_t)(Buffer->size)
	);
}

void * ExpandBuffer( BUFFER *Buffer, int nsize, int nwant )
{
	ALLOC *Alloc = Buffer->Alloc;
	uchar *tmp = Buffer->addr;
	size_t get, had = Buffer->size;

	nwant = (nwant > 0) ? nwant : 0;
	get = nwant * nsize;

	if ( nsize < 1 )
		return NULL;

	if ( get <= had )
	{
		get = Buffer->used * nsize;

		if ( get < had )
			memset( tmp + get, 0, had - get );

		return tmp;
	}

	tmp = Alloc->alloc( Alloc->ud, tmp, had, get );

	if ( !tmp )
		return NULL;

	Buffer->addr = tmp;
	Buffer->size = get;
	Buffer->have = get / nsize;

	return tmp;
}

void * ShrinkBuffer( BUFFER *Buffer, int nsize, int nwant )
{
	ALLOC *Alloc = Buffer->Alloc;
	size_t need;
	void * addr;

	nwant = (nwant > 0) ? nwant : 0;
	need = nsize * nwant;

	if ( Buffer->size < need )
		return NULL;
	else if ( Buffer->size == need )
		return Buffer->addr;

	addr = Alloc->alloc( Alloc->ud, Buffer->addr, Buffer->size, need );

	if ( !need )
	{
		memset( Buffer, 0, sizeof(BUFFER) );
		Buffer->Alloc = Alloc;
	}
	else if ( !addr )
		return NULL;
	else
	{
		Buffer->addr = addr;
		Buffer->size = need;
	}

	return NULL;
}

void * ShrinkBinary( struct _BINARY *Binary, intmax_t want )
{
	BIT Want;
	BUFFER *Buffer = &(Binary->Buffer);

	if ( want < 1 )
		want = 0;

	if ( want > Binary->Used.abs )
		return NULL;

	SetBit( &Want, Buffer->addr, want );
	return ShrinkBuffer( Buffer, 1, want ? Want.pos + !!(Want.mask) : 0 );
}

void * ExpandBinary( struct _BINARY *Binary, intmax_t want )
{
	BIT Used;
	intmax_t Have = Binary->Have.abs;
	uchar *ptr = Binary->Buffer.addr;

	if ( want < 1 )
		return NULL;

	if ( want > Have )
	{
		BIT Want;

		SetBit( &Want, NULL, want );

		ptr = ExpandBuffer( &(Binary->Buffer), 1, Want.pos + 1 );

		if ( !ptr )
			return NULL;

		Want.abs = Binary->Buffer.size;
		Want.abs *= CHAR_BIT;

		SetBit( &(Binary->Used), ptr, Binary->Used.abs );
		SetBit( &(Binary->Have), ptr, Want.abs );
	}

	Used = Binary->Used;

	for ( ; Used.abs < Have; IncBit( &Used, ptr ) )
		ptr[Used.pos] &= ~(Used.mask);

	return ptr;
}

void * ExpandSeries( struct _SERIES *Series, int size, int want )
{
	Series->Buffer.Alloc = Series->Alloc;
	Series->Active.Buffer.Alloc = Series->Alloc;

	if ( !ExpandBinary( &(Series->Active), want ) )
		return NULL;

	return ExpandBuffer( &(Series->Buffer), size, want );
}

int ExtraBuffers( BUFFERS *Buffers, int want )
{
	BUFFER *buffers;
	BINARY *Active = &(Buffers->Active);
	BUFFER *Buffer = &(Buffers->Buffer);
	BIT *Used = &(Active->Used);
	int num = Used->abs + want;

	if ( num < 0 )
		return ERANGE;

	buffers = ExpandSeries( Buffers, sizeof(BUFFER), num );

	if ( !buffers )
		return ENOMEM;

	Buffer->used = num;
	AddBit( Used, Active->Buffer.addr, want );
	return 0;
}

int ObtainBuffer( BUFFERS *Buffers )
{
	BIT pos;
	FILE *errors = Buffers->Alloc->errors;
	BINARY *Active = &(Buffers->Active);
	int num = Active->Used.abs, i = 1;
	BUFFER *buffers = Buffers->Buffer.addr, *buffer;

	for
	(
		SetBit( &pos, Active->Buffer.addr, i );
		i < num;
		++i, IncBit( &pos, Active->Buffer.addr )
	)
	{
		if ( *(pos.byte) & pos.mask )
			continue;

		*((uchar*)(pos.byte)) |= pos.mask;
		buffer = buffers + i;
		buffer->have = 0;
		buffer->used = 0;
		buffer->Alloc = Buffers->Alloc;

		if ( buffer->addr )
			memset( buffer->addr, 0, buffer->size );

		return i;
	}

	++num;

	if ( num > Active->Have.abs )
	{
		int err = ExtraBuffers( Buffers, 32 );

		if ( err )
		{
			ECHO( errors, ECHO_ERR( errors, err ) );
			return -1;
		}

		buffers = Buffers->Buffer.addr;
	}

	IncBit( &(Active->Used), Active->Buffer.addr );
	Buffers->Buffer.used++;

	SetBit( &pos, Active->Buffer.addr, i );
	*((uchar*)(pos.byte)) |= pos.mask;
	buffer = buffers + i;
	buffer->have = 0;
	buffer->used = 0;
	buffer->Alloc = Buffers->Alloc;

	if ( buffer->addr )
		memset( buffer->addr, 0, buffer->size );

	return i;
}


BUFFER * AccessBuffer( BUFFERS *Buffers, int i )
{
	BUFFER * buffers = Buffers->Buffer.addr;
	return (i > 0) ? buffers + i : NULL;
}

void GiveUpBuffer( BUFFERS *Buffers, int i )
{
	BIT pos;
	BINARY *Active = &(Buffers->Active);

	if ( i < 1 || Active->Used.abs <= i )
		return;

	SetBit( &pos, Active->Buffer.addr, i );
	*((uchar*)(pos.byte)) &= ~(pos.mask);
}

void * ShrinkSeries( struct _SERIES *Series, int size, int want )
{
	BINARY *Active = &(Series->Active);
	BUFFER *Buffer = &(Series->Buffer);

	ShrinkBinary( Active, want );
	return ShrinkBuffer( Buffer, size, want );
}

void EmptyBuffers( BUFFERS *Buffers )
{
	int i;
	ALLOC *Alloc = Buffers->Alloc;
	BUFFER *buffers = Buffers->Buffer.addr;

	Buffers->Buffer.Alloc = Alloc;
	Buffers->Active.Buffer.Alloc = Alloc;

	for ( i = 0; i < Buffers->Active.Used.abs; ++i )
	{
		BUFFER *Buffer = buffers + 1;
		Buffer->Alloc = Alloc;
		ShrinkBuffer( Buffer, 1, 0 );
	}

	ShrinkSeries( Buffers, sizeof(BUFFER), 0 );
}
