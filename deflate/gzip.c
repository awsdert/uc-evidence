#include "gzip.h"

int SetupGzipObject( GZIP *gzip, BUFFERS *Buffers )
{
	FILE *errors = Buffers->Alloc->errors;
	STREAM *Stream = &(gzip->Stream);
	ZLIB *zlib = &(gzip->zlib);
	int err;

	gzip->Data.Buffers = Buffers;
	gzip->Data.id = ObtainBuffer( Buffers );
	Stream->data = &(gzip->Data);
	Stream->DataCB = StreamDataFromBufferIndex;

	if ( gzip->Data.id < 0 )
	{
		ECHO( errors, ECHO_ERR( errors, -1 ) );
		return -1;
	}

	gzip->Into = ObtainBuffer( Buffers );

	if ( gzip->Into < 0 )
	{
		ECHO( errors, ECHO_ERR( errors, -1 ) );
		return -1;
	}

	err = SetupZlibObject( zlib, Buffers );
	zlib->Stream = Stream;
	return err;
}

void EchoGzipDetails( FILE *file, GZIP *gzip )
{
	GZIP_FLAGS_UNION *flags = &(gzip->header.flags);

	fprintf
	(
		file,
		"Gzip Header: magic = %04X, "
		"format = %02X, flags = %08X, "
		"xflags = %02X, system = %02X\n"
		"Flag States: TEXT = %s, "
		"HCRC = %s, MORE = %s, "
		"NAME = %s, NOTE = %s, "
		"RESERVED = %X\n"
		"Expected Size = %lu\n",
		gzip->header.magic,
		gzip->header.format,
		flags->val,
		gzip->header.xflags,
		gzip->header.system,
		BOOL2STR( flags->mem.TEXT ),
		BOOL2STR( flags->mem.HCRC ),
		BOOL2STR( flags->mem.MORE ),
		BOOL2STR( flags->mem.NAME ),
		BOOL2STR( flags->mem.NOTE ),
		flags->mem.RESERVED,
		gzip->deflated_size
	);
}

void LoadGzipDetails( GZIP *gzip, ulong end32bits )
{
	ZLIB *zlib = &(gzip->zlib);
	STREAM *Stream = zlib->Stream;
	FILE *verbose = zlib->Buffers->Alloc->verbose;

	if ( verbose )
		fprintf( verbose, "LoadGzipDetails( %p, %lu )\n", (void*)gzip, end32bits );

	gzip->header.magic		= StreamBits( Stream,  8, true ) << 8;
	gzip->header.magic	   |= StreamBits( Stream,  8, true );
	gzip->header.format		= StreamBits( Stream,  8, true );
	gzip->header.flags.val	= StreamBits( Stream,  8, true );
	gzip->header.mtime		= StreamBits( Stream, 32, true );

	FlipBits( &(gzip->header.mtime), 32 );
	gzip->header.xflags		= StreamBits( Stream,  8, true );
	gzip->header.system		= StreamBits( Stream,  8, true );
	gzip->deflated_size		= end32bits ? end32bits : BUFSIZ;

	if ( verbose )
		EchoGzipDetails( verbose, gzip );
}

int LoadGzipArchive( GZIP *gzip, char const *path )
{
	ZLIB *zlib = &(gzip->zlib);
	STREAM *Stream = zlib->Stream;
	BUFFER *Data;
	BUFFERS *Buffers = zlib->Buffers;
	FILE *file = path ? fopen( path, "rb" ) : NULL;
	FILE *errors = Buffers->Alloc->errors;
	FILE *verbose = Buffers->Alloc->verbose;
	size_t size = BUFSIZ;
	intmax_t max = CHAR_BIT;

	if ( !file )
	{
		ECHO( errors, fprintf( errors, "Couldn't open '%s'\n", path ) );
		return EINVAL;
	}

	Data = AccessBuffer( Buffers, gzip->Data.id );

	while ( size == BUFSIZ )
	{
		uint had = Data->size;
		uchar *temp = ExpandBuffer( Data, 1, had + BUFSIZ );

		if ( !temp )
			return ENOMEM;

		memset( temp + had, 0, BUFSIZ );
		size = fread( temp + had, 1, BUFSIZ, file );
		Data->size += BUFSIZ;
		Data->have += BUFSIZ;
		Data->used += size;
		Data->addr = temp;
	}

	fclose( file );
	/* We use NULL here so that we can just add to the original */
	max *= Data->used;
	size = 0;
	BackBits( &(size), Data->addr, max - 1, 32 );
	LoadGzipDetails( gzip, size );

	if ( verbose )
	{
		ECHO
		(
			verbose,
			fprintf( verbose, "max = %" PRIdMAX "\n", max );
			EchoStreamDetails( verbose, Stream )
		);
	}

	return 0;
}

int ExpandGzipArchive( GZIP *gzip )
{
	int err;
	ZLIB *zlib = &(gzip->zlib);
	STREAM *Stream = zlib->Stream;
	BUFFERS *Buffers = zlib->Buffers;
	BUFFER *Into = AccessBuffer( Buffers, gzip->Into );
	uchar *into = ExpandBuffer( Into, 1, gzip->deflated_size + 1 );
	FILE *verbose = Buffers->Alloc->verbose;

	if ( !into || !gzip->deflated_size )
		return ENOMEM;

	if ( gzip->header.flags.mem.NAME )
	{
		size_t size = 1;

		while ( size && Stream->err != EOF )
			size = StreamBits( Stream, 8, true );
	}

	err = ExpandZlibStream( zlib, Stream, gzip->Into );

	if ( verbose )
	{
		size_t byte, cap = Into->used;
		cap = (cap > 32) ? 32 : cap;
		into = Into->addr;
		ECHO
		(
			verbose,
			fprintf( verbose, "Zlib Expansion Result:\n%s\n", (char*)into );
			for ( byte = 0; byte < cap; ++byte )
				fprintf( verbose, " %02X", into[byte] );
			fputc( '\n', verbose )
		);
	}

	return err;
}
