#ifndef INC_COMMON_H
#define INC_COMMON_H

#include <unistd.h>
#include <limits.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <inttypes.h>
#include <errno.h>
#include <malloc.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#ifndef bitsof
#define bitsof(T) (sizeof(T) * CHAR_BIT)
#endif

typedef   signed char		schar;
typedef unsigned char		uchar;
typedef unsigned short	int	ushort;
typedef unsigned int		uint;
typedef unsigned long	int	ulong;

#ifdef ULLONG_MAX
#define LLONG long long
typedef   signed LLONG	int llong;
typedef unsigned LLONG	int	ullong;
#endif

#define BOOL2STR( VAL ) ((VAL) ? "true" : "false")

#define _ECHO( PATH, LINE, FILE, CODE ) \
	if ( 1 ) { fprintf( FILE, "%s:%u: ", PATH, LINE ); CODE; }

#define ECHO( FILE, CODE ) _ECHO( __FILE__, __LINE__, FILE, CODE )

#define ECHO_ERR( FILE, ERR ) \
	fprintf( FILE, "Error 0x%08X (%d) '%s'\n", (ERR), (ERR), strerror(ERR) )

#endif
