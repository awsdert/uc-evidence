#include "zlib.h"
#include <stdlib.h>

bool quiet_zlib = false;

ZLIB_IMPLIED implied_type_data =
{
	/* get extra bits */
	{
		0, 0, 0, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0,
		2, 3, 7, 0, 0, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0
	},
	/* base cpy value */
	{
		0, 0,  0, 0, 0, 0, 0, 0,
		0, 0,  0, 0, 0, 0, 0, 0,
		3, 3, 11, 0, 0, 0, 0, 0,
		0, 0,  0, 0, 0, 0, 0, 0
	}
};

ZLIB_IMPLIED implied_code_data =
{
	/* get extra bits */
	{
		0, 0, 0, 0, 0, 0, 0, 0,
		1, 1, 1, 1, 2, 2, 2, 2,
		3, 3, 3, 3,	4, 4, 4, 4,
		5, 5, 5, 5, 0, 0, 0, 0
	},
	/* base cpy value */
	{
		  3,   4,   5,   6,   7,   8,   9,  10,
		 11,  13,  15,  17,  19,  23,  27,  31,
		 35,  43,  51,  59,  67,  83,  99, 115,
		131, 163, 195, 227, 258,   0,   0,   0,
	}
};

ZLIB_IMPLIED implied_loop_data =
{
	/* get extra bits */
	{
		 0,  0,  0,  0,  1,  1,  2,  2,
		 3,  3,  4,  4,  5,  5,  6,  6,
		 7,  7,  8,  8,  9,  9, 10, 10,
		11, 11, 12, 12, 13, 13,	 0,  0
	},
	/* base cpy value */
	{
			 1,    2,    3,     4,     5,     7,    9,    13,
			17,   25,   33,    49,    65,    97,  129,   193,
		   257,  385,  513,   769,  1025,  1537, 2049,  3073,
		  4097, 6145, 8193, 12289, 16385, 24577,    0,     0
	}
};

int SetupZlibObject( ZLIB *zlib, BUFFERS *Buffers )
{
	int id;
	FILE *errors = Buffers->Alloc->errors;

	zlib->Buffers = Buffers;

	for ( id = 0; id < ZLIB_SYMBOLS_ID_COUNT; ++id )
	{
		ZLIB_SYMBOLS *Symbols = &(zlib->Symbols[id]);
		BUFFER *Entries;
		ulong get = (id == ZLIB_SYMBOLS_ID_CODE) ? 320 : 32;

		Symbols->entries = ObtainBuffer( Buffers );
		Symbols->Buffers = Buffers;

		if ( Symbols->entries < 0 )
		{
			ECHO( errors, ECHO_ERR( errors, ENOMEM ) );
			return ENOMEM;
		}

		Entries = AccessBuffer( Buffers, Symbols->entries );

		if ( !ExpandBuffer( Entries, sizeof(ZLIB_SYMBOL), get ) )
		{
			ECHO( errors, ECHO_ERR( errors, ENOMEM ) );
			return ENOMEM;
		}
	}

	return 0;
}

void EmptyZlibObject( ZLIB *zlib )
{
	int id;

	for ( id = 0; id < ZLIB_SYMBOLS_ID_COUNT; ++id )
	{
		ZLIB_SYMBOLS *Symbols = &(zlib->Symbols[id]);
		GiveUpBuffer( zlib->Buffers, Symbols->entries );
	}

	memset( zlib, 0, sizeof(ZLIB) );
}

void * ExpandZlibSymbolsBuffer( ZLIB_SYMBOLS *Symbols, uint want )
{
	BUFFER *Entries = AccessBuffer( Symbols->Buffers, Symbols->entries );
	return ExpandBuffer( Entries, sizeof(ZLIB_SYMBOL), want );
}

int CompareZlibSymbols( void const *A, void const *B )
{
	ZLIB_SYMBOL const *a = A, *b = B;

	if ( a->use != b->use )
		return b->use - a->use;

	if ( a->len != b->len )
		return a->len - b->len;

	return a->uid - b->uid;
}

void InitZlibSymbolUIDs( ZLIB_SYMBOLS *Symbols )
{
	uint uid = 0, len;
	BUFFERS *Buffers = Symbols->Buffers;
	BUFFER *Entries = AccessBuffer( Buffers, Symbols->entries );
	ZLIB_SYMBOL *symbols = Entries->addr, *symbol;

	for ( len = 1; len <= Symbols->longest; ++len )
	{
		int i;
		for ( i = 0; i < Entries->used; ++i )
		{
			symbol = symbols + i;

			if ( symbol->len != len )
				continue;

			symbol->uid = uid++;
			Symbols->highest = uid;
		}

		uid <<= 1;
	}
}

ZLIB_SYMBOL * SeekZlibSymbol( STREAM *Stream, ZLIB_SYMBOLS *Symbols )
{
	BUFFERS *Buffers = Symbols->Buffers;
	BUFFER *Entries = AccessBuffer( Buffers, Symbols->entries );
	ZLIB_SYMBOL *symbols = Entries->addr;
	FILE *errors = Buffers->Alloc->errors;
	FILE *detailed = Buffers->Alloc->detailed;
	uint len, uid = 0;

	for ( len = 1; len <= Symbols->longest; ++len )
	{
		ZLIB_SYMBOL *symbol;

		uid = StreamBits( Stream, len, false );
		FlipBits( &uid, len );
		symbol = symbols + uid;

		if ( (int)uid >= Entries->used )
			return NULL;

		if ( symbol->len != len )
			continue;

		if ( !uid || (symbol->use && symbol->uid == uid) )
		{
			StreamUsed( Stream, len );
			return symbol;
		}
	}

	ECHO
	(
		errors,
		fprintf( errors, "Unable to identify symbol, last UID sought: " );
		EchoBits( detailed, &uid, 0, len );
		putchar('\n')
	);

	return NULL;
}

int SortZlibSymbolsByUID( ZLIB_SYMBOLS *Symbols )
{
	uint j = 0;
	BUFFERS *Buffers = Symbols->Buffers;
	BUFFER *Entries = AccessBuffer( Buffers, Symbols->entries );
	ZLIB_SYMBOL *symbols = Entries->addr, *dst, *src;

	qsort( symbols, Entries->used, sizeof(ZLIB_SYMBOL), CompareZlibSymbols );

	symbols =
		ExpandBuffer( Entries, sizeof(ZLIB_SYMBOL), Symbols->highest + 1 );

	if ( !symbols )
		return ENOMEM;

	for ( j = Entries->used - 1; j; --j )
	{
		src = symbols + j;
		dst = symbols + src->uid;

		if ( src->uid > j )
		{
			*dst = *src;
			memset( src, 0, sizeof(ZLIB_SYMBOL) );
		}
	}

	Entries->used = Symbols->highest + 1;

	return 0;
}

void * LoadZlibTypes( ZLIB *zlib )
{
	BUFFERS *Buffers = zlib->Buffers;
	ZLIB_SYMBOLS_ID id = ZLIB_SYMBOLS_ID_TYPE;
	ZLIB_SYMBOLS *Types = &(zlib->Symbols[id]);
	BUFFER *Entries = AccessBuffer( Buffers, Types->entries );
	int i = Types->foresee;
	ZLIB_SYMBOL *types = ExpandZlibSymbolsBuffer( Types, i < 32 ? 32 : i + 1 );
	FILE *detailed = Buffers->Alloc->detailed;

	if ( !types )
		return NULL;

	for ( i = 0; i < Types->foresee; ++i )
	{
		static uint adjust[] =
		{
			16, 17, 18, 0,  8, 7,
			 9,  6, 10, 5, 11, 4,
			12,  3, 13, 2, 14, 1,
			15, 19
		};

		uint pos = (i < 20) ? adjust[i] : 31;

		ZLIB_SYMBOL *type = types + pos;

		type->src = pos;
		type->get = implied_type_data.get[pos];
		type->cpy = implied_type_data.cpy[pos];
		type->len = StreamBits( zlib->Stream, 3, true );
		type->use = !!(type->len);

		if ( type->len > Types->longest )
			Types->longest = type->len;
	}

	Entries->used = 19;

	InitZlibSymbolUIDs( Types );

	SortZlibSymbolsByUID( Types );

	return types;
}

void * LoadZlibHuffs( ZLIB *zlib, uint starting_lit )
{
	uint lit = starting_lit;
	ZLIB_SYMBOLS_ID id = lit ? ZLIB_SYMBOLS_ID_LOOP : ZLIB_SYMBOLS_ID_CODE;
	ZLIB_SYMBOLS *Types = &(zlib->Symbols[ZLIB_SYMBOLS_ID_TYPE]);
	ZLIB_SYMBOLS *Symbols = &(zlib->Symbols[id]);
	int pos = 0, used = 0, stop = Symbols->foresee;
	BUFFER *Entries = AccessBuffer( zlib->Buffers, Symbols->entries );
	ZLIB_SYMBOL *symbols, *symbol, *previous = NULL;
	STREAM *Stream = zlib->Stream;
	FILE *errors = zlib->Buffers->Alloc->errors;
	FILE *verbose = zlib->Buffers->Alloc->verbose;
	FILE *detailed = zlib->Buffers->Alloc->detailed;
#
	if ( !quiet_zlib )
	{
		ECHO
		(
			verbose,
			fprintf
			(
				verbose,
				"LoadZlibHuffs( %p, %u )\n",
				(void*)zlib, starting_lit
			)
		);
	}

	symbols = ExpandZlibSymbolsBuffer( Symbols, stop + 1 );

	if ( !symbols )
	{
		ECHO
		(
			errors,
			fputs( "Couldn't allocate memory for symbols\n", errors );
			ECHO_ERR( errors, ENOMEM )
		);
		return NULL;
	}

	memset( symbols, 0, Entries->size );

	while ( pos < stop && Stream->err != EOF )
	{
		int i = 0, copy = 0;
		uintmax_t val;
		ZLIB_SYMBOL *type = SeekZlibSymbol( Stream, Types );

		if ( !type )
		{
			ECHO
			(
				errors,
				fputs( "Couldn't find symbol!\n", errors );
				EchoZlibSymbolsListDetails( errors, zlib, ZLIB_SYMBOLS_ID_TYPE );
				ECHO_ERR( errors, EINVAL )
			);
			return NULL;
		}

		if ( !quiet_zlib )
		{
			ECHO
			(
				detailed,
				EchoStreamDetails( detailed, Stream );
				fprintf( detailed,"type_" );
				EchoZlibSymbolDetails( detailed, type, type->src )
			);
		}

		if ( type->src >= 16 )
		{
			val = StreamBits( Stream, type->get, true );
			copy = (uint)(type->cpy + val);

			if ( type->src > 16 || !(previous->len) )
			{
				lit += copy;
				pos += copy;
				copy = 0;
			}
		}
		else
		{
			symbol = symbols + pos;

			symbol->src = pos;
			symbol->lit = lit;

			if ( starting_lit )
			{
				symbol->use = true;
				symbol->len = type->len + 1;
				symbol->get = implied_loop_data.get[pos];
				symbol->cpy = implied_loop_data.cpy[pos];
			}
			else
			{
				symbol->use = !!(type->src);
				symbol->len = type->src;
				symbol->get = 0;
				symbol->cpy = (lit > 256) ? used : 0;
			}

			if ( !quiet_zlib && detailed )
			{
				fprintf(detailed,"%s_", starting_lit ? "loop" : "code" );
				EchoZlibSymbolDetails( detailed, symbol, pos );
			}

			if ( symbol->len > Symbols->longest )
				Symbols->longest = symbol->len;

			previous = symbol;
			++used;
			++pos;
			++lit;
		}

		for ( i = 0; i < copy; ++i, ++pos, ++lit )
		{
			symbol = symbols + pos;

			symbol->src = pos;
			symbol->lit = lit;
			symbol->use = true;
			symbol->len = previous->len;

			if ( !quiet_zlib && detailed )
			{
				fprintf(detailed,"%s_", starting_lit ? "loop" : "code" );
				EchoZlibSymbolDetails( detailed, symbol, pos );
			}

			++used;
		}
	}

	Entries->used = pos;

	InitZlibSymbolUIDs( Symbols );

	SortZlibSymbolsByUID( Symbols );

	return symbols;
}

void EchoZlibSymbolDetails( FILE *dst, ZLIB_SYMBOL *symbol, uint pos )
{
	fprintf
	(
		dst,
		"symbols[%7u]: "
		"src = %3u, "
		"lit = %3u, "
		"get = %2u, "
		"cpy = %5u, "
		"len = %2u, "
		"use = %5s, "
		"uid = ",
		pos,
		symbol->src,
		symbol->lit,
		symbol->get,
		symbol->cpy,
		symbol->len,
		BOOL2STR( symbol->use )
	);
	EchoBits( dst, &(symbol->uid), 0, symbol->len );
	putchar('\n');
}

void EchoZlibSymbolsListDetails( FILE *dst, ZLIB *zlib, int id )
{
	ZLIB_SYMBOLS *Symbols = &(zlib->Symbols[id]);
	BUFFER *Entries = AccessBuffer( zlib->Buffers, Symbols->entries );
	char *names[ZLIB_SYMBOLS_ID_COUNT] = {NULL};
	names[ZLIB_SYMBOLS_ID_TYPE] = "Type";
	names[ZLIB_SYMBOLS_ID_CODE] = "Code";
	names[ZLIB_SYMBOLS_ID_LOOP] = "Dist";

	fprintf
	(
		dst,
		"%s Symbols List Details: "
		"foresee: %3u, longest = %2u, highest = %2u\n",
		names[id], Symbols->foresee, Symbols->longest, Symbols->highest
	);

	EchoBufferDetails( dst, Entries );

	if ( 1 )
	{
		int i;
		ZLIB_SYMBOL *symbols = Entries->addr;

		for ( i = 0; i < Entries->used; ++i )
		{
			ZLIB_SYMBOL *symbol = symbols + i;

			if ( symbol->use )
				EchoZlibSymbolDetails( dst, symbol, i );
		}
	}
}

int ExpandZlibStreamType0( ZLIB *zlib )
{
	STREAM *Stream = zlib->Stream;
	BUFFER *Into = AccessBuffer( zlib->Buffers, zlib->Into );
	uchar *into = Into->addr;
	uint i, leng, nlen, left = Stream->used % 8;
	FILE *errors  = zlib->Buffers->Alloc->errors;
	FILE *verbose  = zlib->Buffers->Alloc->verbose;
	FILE *detailed  = zlib->Buffers->Alloc->detailed;
	size_t size;

	if ( left )
		StreamBits( Stream, 8 - left, true );

	leng = StreamBits( Stream, 16, true );
	nlen = StreamBits( Stream, 16, true );

	if ( verbose && !quiet_zlib )
	{
		ECHO
		(
			verbose,
			fprintf( verbose, "leng = %u, nlen = %u\n", leng, nlen )
		);
	}

	size = Into->size / Into->have;
	into = ExpandBuffer( Into, size, leng );

	if ( !into )
	{
		ECHO( errors, ECHO_ERR( errors, ENOMEM ) );
		return ENOMEM;
	}

	for ( i = 0; i < leng && Stream->err != EOF; ++i )
	{
		uchar c = (uchar)StreamBits( Stream, 8, true );

		if ( detailed && !quiet_zlib )
			fprintf( detailed, " %02X", c );

		if ( Into->used == Into->have )
		{
			into = ExpandBuffer( Into, size, Into->used + BUFSIZ );

			if ( !into )
			{
				ECHO( errors, ECHO_ERR( errors, ENOMEM ) );
				return ENOMEM;
			}
		}

		into[Into->used++] = c;
	}

	if ( detailed && !quiet_zlib )
		fputc( '\n', detailed );

	return 0;
}

int ExpandZlibStreamType1( ZLIB *zlib )
{
	STREAM *Stream = zlib->Stream;
	BUFFER *Into = AccessBuffer( zlib->Buffers, zlib->Into );
	uchar *into = Into->addr;
	uint lookup = 0, search = 0, i = Into->used;
	FILE *detailed = zlib->Buffers->Alloc->detailed;

	while ( Stream->err != EOF )
	{
		uint lit = StreamBits( Stream, 8, false );
		FlipBits( &lit, 8 );

		if ( lit >= 48 )
		{
			lit -= 48;
			StreamUsed( Stream, 8 );

			into = ExpandBuffer( Into, 1, i + 1 );

			if ( !into )
				return ENOMEM;

			into[i++] = (uchar)lit;
			lookup = i;

			if ( detailed && !quiet_zlib )
			{
				ECHO
				(
					detailed,
					fprintf
					(
						detailed,
						"lit = %02X\n",
						lit
					)
				);
			}
		}
		else
		{
			uint leng = 0, li = 0, l = 0;
			uint loop = 0, di = 0;
			uint bits = 0;

			li = StreamBits( Stream, 7, true );
			FlipBits( &li, 7 );
			li--;
			lit = li + 257;

			if ( lit == 256 )
			{
				if ( detailed && !quiet_zlib )
				{
					ECHO
					(
						detailed,
						fprintf
						(
							detailed,
							"lit = %3u, li = %2u, breaking...\n",
							lit, li
						)
					);
				}
				break;
			}

			bits = implied_code_data.get[li];
			leng = StreamBits( Stream, bits, true );
			FlipBits( &leng, bits );
			leng += implied_code_data.cpy[li];

			if ( detailed && !quiet_zlib )
			{
				ECHO
				(
					detailed,
					fprintf
					(
						detailed,
						"lit = %u, "
						"li = %2u, leng = %3u, "
						"di = %2u, loop = %3u\n",
						lit, li, leng, di, loop
					);
					EchoStreamDetails( detailed, Stream )
				);
			}

			di = StreamBits( Stream, 5, true );
			FlipBits( &di, 5 );

			bits = implied_loop_data.get[di];
			loop = StreamBits( Stream, bits, true );
			FlipBits( &loop, bits );
			loop += implied_loop_data.cpy[di];

			if ( loop > lookup )
				return EINVAL;

			search = lookup - loop;

			if ( !quiet_zlib )
			{
				ECHO
				(
					detailed,
					fprintf
					(
						detailed,
						"lit = %u, "
						"li = %2u, leng = %3u, "
						"di = %2u, loop = %3u\n",
						lit, li, leng, di, loop
					);
					EchoStreamDetails( detailed, Stream )
				);
			}

			into = ExpandBuffer( Into, 1, i + leng + 1 );

			if ( !into )
				return ENOMEM;

			for ( l = 0; l < leng; ++l, ++search )
			{
				lit = (uchar)(into[search]);
				into[i++] = lit;

				if ( !quiet_zlib )
				{
					ECHO
					(
						detailed,
						fprintf( detailed, "lit = %u, l = %3u\n", lit, l )
					);
				}
			}

			lookup = i;
		}
	}

	Into->used = i;

	return 0;
}

int ExpandZlibStreamType2( ZLIB *zlib )
{
	STREAM *Stream = zlib->Stream;
	BUFFERS *Buffers = zlib->Buffers;
	BUFFER *Into = AccessBuffer( Buffers, zlib->Into );
	uchar *into = Into->addr;
	int i = 0, j = 0;
	ZLIB_SYMBOLS *Types = &(zlib->Symbols[ZLIB_SYMBOLS_ID_TYPE]);
	ZLIB_SYMBOLS *Codes = &(zlib->Symbols[ZLIB_SYMBOLS_ID_CODE]);
	ZLIB_SYMBOLS *Loops = &(zlib->Symbols[ZLIB_SYMBOLS_ID_LOOP]);
	ZLIB_SYMBOL *types, *Code, *Loop;
	FILE *errors = Buffers->Alloc->errors;
	FILE *verbose = Buffers->Alloc->verbose;

	Codes->foresee = StreamBits( Stream, 5, true ) + 257;
	Loops->foresee = StreamBits( Stream, 5, true ) + 1;
	Types->foresee = StreamBits( Stream, 4, true ) + 4;

	if ( verbose && !quiet_zlib )
	{
		fprintf( verbose, "Deflation Type 2: \n" );

		fprintf( verbose, "Code " );
		EchoZlibSymbolsListDetails( verbose, zlib, ZLIB_SYMBOLS_ID_CODE );

		fprintf( verbose, "Loop " );
		EchoZlibSymbolsListDetails( verbose, zlib, ZLIB_SYMBOLS_ID_LOOP );

		fprintf( verbose, "Type " );
		EchoZlibSymbolsListDetails( verbose, zlib, ZLIB_SYMBOLS_ID_TYPE );
	}

	types = LoadZlibTypes( zlib );

	if ( !types )
		return -1;

	if ( !quiet_zlib )
	{
		ECHO
		(
			verbose,
			EchoStreamDetails( verbose, Stream );
			EchoZlibSymbolsListDetails( verbose, zlib, ZLIB_SYMBOLS_ID_TYPE )
		);
	}

	Code = LoadZlibHuffs( zlib, 0 );

	if ( !Code )
		return -1;

	if ( verbose && !quiet_zlib )
	{
		ECHO
		(
			verbose,
			EchoStreamDetails( verbose, Stream );
			EchoZlibSymbolsListDetails( verbose, zlib, ZLIB_SYMBOLS_ID_CODE )
		);
	}

	Loop = LoadZlibHuffs( zlib, Codes->foresee );

	if ( !Loop )
		return -1;

	if ( verbose && !quiet_zlib )
	{
		ECHO
		(
			verbose,
			EchoStreamDetails( verbose, Stream );
			EchoZlibSymbolsListDetails( verbose, zlib, ZLIB_SYMBOLS_ID_LOOP );
		);
	}

	i = Into->used; j = 0;
	while ( Stream->err != EOF )
	{
		Code = SeekZlibSymbol( Stream, Codes );

		if ( !Code )
			return EINVAL;

		if ( Code->lit == 256 )
			break;
		else if ( Code->lit < 256 )
		{
			j = i++;
			Into->used = i;
			into = ExpandBuffer( Into, 1, i );

			if ( !into )
				return ENOMEM;

			into[j] = Code->lit;
		}
		else
		{
			int lookup;
			int repeat;

			Loop = SeekZlibSymbol( Stream, Loops );

			if ( !Loop )
				return EINVAL;

			repeat = Code->cpy;
			lookup = Loop->cpy;
			lookup += StreamBits( Stream, Loop->get, true );
			lookup = i - lookup;

			Into->used = i;
			into = ExpandBuffer( Into, 1, i + repeat + 1 );

			if ( !into )
				return ENOMEM;

			if ( lookup > Into->have )
				lookup = 0;

			for ( j = 0; j < repeat; ++j )
			{
				if ( (uint)i == Into->size )
				{
					ECHO
					(
						errors,
						fprintf
						(
							errors,
							"Into->size = %lu\n",
							(ulong)(Into->size)
						)
					);
					return ERANGE;
				}

				into[i++] = into[lookup++];
			}

			j = i - 1;
		}
	}

	return 0;
}

int ExpandZlibStreamType3( ZLIB *zlib ) { (void)zlib; return EINVAL; }

ExpandZlibStreamTypeCB ExpandZlibStreamType[4] =
{
	ExpandZlibStreamType0,
	ExpandZlibStreamType1,
	ExpandZlibStreamType2,
	ExpandZlibStreamType3
};

int ExpandZlibStream( ZLIB *zlib, STREAM *Stream, int Into )
{
	bool last = false;
	FILE *verbose = zlib->Buffers->Alloc->errors;
	zlib->Into = Into;
	zlib->Stream = Stream;

	while ( !last && Stream->err != EOF )
	{
		int err = 0;
		uint type;

		last = StreamBits( Stream, 1, true );
		type = StreamBits( Stream, 2, true );

		if ( verbose && !quiet_zlib )
		{
			ECHO
			(
				verbose,
				fprintf
				(
					verbose,
					"last = %u, type = %u%u, Stream->used = %" PRIdMAX "\n",
					last, type >> 1, type & 1, Stream->used
				)
			);
		}

		err = ExpandZlibStreamType[type]( zlib );

		if ( err )
			return err;
	}

	return 0;
}
