#include "gzip.h"

char const *archive = NULL;

int ArgHasIssue( char *arg, char *val )
{
	if ( DefaultAlloc.verbose )
	{
		ECHO
		(
			DefaultAlloc.verbose,
			fprintf( DefaultAlloc.verbose, "ArgHasIssue( '%s', '%s' )\n", arg, val )
		);
	}

	if ( !val )
	{
		val = strchr( arg, '=' );

		if ( val )
			++val;
	}

	if ( strstr( arg, "--verbose" ) )
	{
		DefaultAlloc.verbose = (val && strcmp( val, "2" ) == 0) ? stderr : stdout;
		DefaultAlloc.errors = DefaultAlloc.verbose;
		ECHO
		(
			DefaultAlloc.verbose,
			fprintf( DefaultAlloc.verbose, "ArgHasIssue( '%s', '%s' )\n", arg, val )
		);
		return 0;
	}

	if ( strstr( arg, "--detailed" ) )
	{
		if ( !DefaultAlloc.verbose )
		{
			DefaultAlloc.verbose = (val && strcmp( val, "2" ) == 0) ? stderr : stdout;
			DefaultAlloc.errors = DefaultAlloc.verbose;
			ECHO
			(
				DefaultAlloc.verbose,
				fprintf( DefaultAlloc.verbose, "ArgHasIssue( '%s', '%s' )\n", arg, val )
			);
		}

		DefaultAlloc.detailed = DefaultAlloc.verbose;
		return 0;
	}

	if ( arg[1] == 'f' )
	{
		if ( !val )
		{
			ECHO
			(
				DefaultAlloc.errors,
				fprintf( DefaultAlloc.errors, "No path declared '%s'\n", val )
			)
			return EINVAL;
		}

		if ( access(val, 0) != 0 )
		{
			ECHO
			(
				DefaultAlloc.errors,
				fprintf( DefaultAlloc.errors, "Couldn't find '%s'\n", val )
			)
			return EACCES;
		}

		archive = val;
		return 0;
	}

	return EINVAL;
}

int ArgHadIssue( int err, char *arg )
{
	FILE *errors = DefaultAlloc.errors;
	ECHO( errors, ECHO_ERR( errors, err ) );
	ECHO( errors, fprintf( errors, "Invalid argument '%s'\n", arg ) );
	fprintf( errors, "Usage: APP [--verbose,--detailed,-f PATH]\n" );
	return EXIT_FAILURE;
}

int ProcessArgv( int argc, char **argv )
{
	int a;
	char *prv = NULL;

	for ( a = 0; a < argc; ++a )
	{
		int err;
		char *arg = argv[a];

		if ( !arg )
			continue;

		if ( arg[0] == '-' )
		{
			if ( prv )
			{
				err = ArgHasIssue( prv, NULL );

				if ( err )
					return ArgHadIssue( err, arg );
			}

			if ( strchr( arg, '=' ) )
			{
				err = ArgHasIssue( arg, NULL );

				if ( err )
					return ArgHadIssue( err, arg );

				continue;
			}

			prv = arg;
			continue;
		}

		if ( !prv )
		{
			if ( a > 0 )
				return ArgHadIssue( EINVAL, arg );
			continue;
		}

		err = ArgHasIssue( prv, arg );

		if ( err )
			return ArgHadIssue( err, arg );

		prv = NULL;
	}

	return 0;
}

int Main( GZIP *gzip, BUFFERS *Buffers )
{
	int err = SetupGzipObject( gzip, Buffers );
	FILE *errors = DefaultAlloc.errors;

	if ( err )
	{
		ECHO( errors, ECHO_ERR( errors, err ) );
		return err;
	}

	err = LoadGzipArchive( gzip, archive );

	if ( err )
	{
		ECHO( errors, ECHO_ERR( errors, err ) );
		return err;
	}

	return ExpandGzipArchive( gzip );
}

int main( int argc, char *argv[] )
{
	int ret = EXIT_FAILURE, err;
	GZIP gzip = {0};
	BUFFERS Buffers = {0};

	#ifdef _DEBUG
	DefaultAlloc.errors = stderr;
	#else
	DefaultAlloc.errors = stdout;
	#endif

	Buffers.Alloc = &DefaultAlloc;

	err = ExtraBuffers( &Buffers, 32 );

	if ( err )
	{
		ECHO( DefaultAlloc.errors, ECHO_ERR( DefaultAlloc.errors, ret ) );
		return ret;
	}

	if ( ProcessArgv( argc, argv ) != 0 )
		return ret;

	err = Main( &gzip, &Buffers );
	EmptyBuffers( &Buffers );

	if ( err )
		ECHO( DefaultAlloc.errors, ECHO_ERR( DefaultAlloc.errors, err ) );

	return err ? EXIT_FAILURE : EXIT_SUCCESS;
}
